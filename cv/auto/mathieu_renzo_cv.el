(TeX-add-style-hook
 "mathieu_renzo_cv"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("scrartcl" "11pt" "letter")))
   (TeX-run-style-hooks
    "latex2e"
    "setup"
    "scrartcl"
    "scrartcl11"))
 :latex)

