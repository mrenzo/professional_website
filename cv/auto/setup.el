(TeX-add-style-hook
 "setup"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("hyperref" "colorlinks" "citecolor=blue!80!black" "linkcolor=blue!80!black" "urlcolor=blue!80!black") ("textpos" "absolute" "overlay")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "geometry"
    "fancyhdr"
    "epsfig"
    "amsmath"
    "amsfonts"
    "amssymb"
    "listings"
    "booktabs"
    "setspace"
    "hyperref"
    "xcolor"
    "lipsum"
    "wrapfig"
    "xhfill"
    "sectsty"
    "pgfgantt"
    "multicol"
    "textpos"
    "caption"
    "fontawesome5"
    "longtable"
    "tabu"
    "eurosym"
    "etaremune"
    "tcolorbox"
    "mdframed")
   (TeX-add-symbols
    '("subSection" 1)
    '("Icon" 1)
    '("Secref" 1)
    '("Tabref" 1)
    '("Figref" 1)
    '("Eqref" 1)
    '("sectionLine" 1)
    '("highlight" 1)
    '("todo" 1)
    "udef"
    "tempone"
    "temptwo"
    "boxitem")
   (LaTeX-add-counters
    "TODOLIST")
   (LaTeX-add-mdframed-mdfdefinestyles
    "Framed"))
 :latex)

