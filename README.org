#+title: Code for Mathieu Renzo's website

I write the content in org mode (see [[./org-content/]]), and use emacs to
export to HTML. The export is done by emacs (see [[./build-site.el]]).
This repo is cloned on the server, where I run [[./build.sh]] to clone the
repo and export org files to HTML, and the HTML files are hyper-linked in a
folder accessible from the web.

The folders [[./materials/]], [[./images/]], [[./html-templates/]] and [[./css/]]
contain all the "assets" and can be linked in the org files, the links
will properly be translated into HTML links at export.

** Typical workflow


 This procedure requires a working emacs installation on the server
 ([[./build-site.el]] will also install *locally* in [[./.packages/]] the
 necessary emacs packages, namely =htmlize= and =ox-publish=, without
 messing with any other existing emacs installations).

 1. edit [[./org-content/]] local machine and push to repo
 2. on the server, run [[./build.sh]] which pulls from the repo, and using
    emacs converts =org-content/*.org= to HTML files
 3. hyper-link the produced HTML files where they can be accessed (to
    be done once the first time only)
